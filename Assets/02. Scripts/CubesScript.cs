﻿using UnityEngine;
using System.Collections;

public class CubesScript : MonoBehaviour {

	public KeyCode key = KeyCode.A;

	public GameObject[] cubes;





	// Use this for initialization
	void Start () {
		cubes = GameObject.FindGameObjectsWithTag ("Cubes");
	
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(key)){
			foreach(GameObject cube in cubes){				
				MeshRenderer cubeMeshR = cube.GetComponent<MeshRenderer> ();
				cubeMeshR.material.color = Color.red;
			}
		}
	
	}

}
