﻿using UnityEngine;
using System.Collections;


public class MenuScript : MonoBehaviour {


	public GameObject spaceShip;
	public GameObject cube;

	[Range(10.0f, 100.0f)]
	public float speedRotation = 30.0f;

	public GameObject[] simonLight;



	public float initSimonTime = 0.5f;

	public float frequencySimonLight = 1f;

	public float lightOnTime = 0.5f;


	// Use this for initialization
	void Start () {
		Debug.Log ("MenuScript_Start");
	
		simonLight = GameObject.FindGameObjectsWithTag ("SimonLight");
		foreach (GameObject light in simonLight) {
			light.SetActive(false);
		}


		//SpaceShip = GameObject.Find ("SpaceShip");
		//if (SpaceShip == null) {
			//Debug.LogError ("Error");
			//Debug.LogWarning ("Warning");
		//}

		InvokeRepeating ("LightUp", initSimonTime, frequencySimonLight);


	}
	
	// Update is called once per frame
	void Update () {

		cube.transform.Rotate (0.0f, speedRotation * Time.deltaTime, 0.0f);

		if (Input.GetKeyDown (KeyCode.A)) {
			spaceShip.SetActive (!spaceShip.activeSelf);
		}

	}

	void LightUp(){		

		int rand = Random.Range (0, simonLight.Length - 1);
		simonLight [rand].SetActive (true);
		Invoke ("LightDown", lightOnTime);
	}

	void LightDown(){
		foreach (GameObject light in simonLight) {
			light.SetActive(false);
		}
	}






}
